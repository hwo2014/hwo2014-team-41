args <- commandArgs(trailingOnly = TRUE)

data = read.csv(args[1])
pdf("ai.pdf")

layout(matrix(c(1,2)))
par(xpd=TRUE)
# Desired and current speeds
plot(data$desired, col="red", ylim=c(0,10), main="Desired and Current speeds")
lines(data$current, col="green", type="l")
legend("topright", c("Desired", "Current"), fill=c("red", "green"), inset=c(0, -0.4))

# Control variable and throttle
plot(data$cv, col="red", type="l", main="Throttle and control variable")
par(new=TRUE)
plot(data$throttle, col="green", type="l", axes=F, ylab="n")
axis(4)
legend("topright", c("CV", "Throttle"), fill=c("red", "green"), inset=c(0, -0.4))
dev.off()
