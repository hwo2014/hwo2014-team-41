import json
import socket
import sys
import logging
import logging.config

from tracks.track import Track
from car import Car
from ai import AI

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.logger = logging.getLogger("console")
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    # Game events start here

    def on_join(self, data):
        self.logger.info("Joined")
        self.ping()

    def on_game_init(self, data):
        self.track = Track(data["race"]["track"])
        self.car = Car(self.name, self.track)
        self.ai = AI(self.car, self.track)
        self.logger.info("Game objects initialized")

    def on_game_start(self, data):
        self.logger.info("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.logger.debug("-----------------------------------------------------")
        [self.car.update(raw) for raw in data if raw["id"]["name"] == self.name]

        action = self.ai.update()
        if action is not None:
            self.logger.debug("Going to do: {}".format(action))
            self.msg(action[0], action[1])
        else:
            self.ping()

    def on_crash(self, data):
        self.logger.info("Someone crashed: {}".format(data))
        self.ping()

    def on_spawn(self, data):
        self.logger.info("Someone spawned: {}".format(data))
        self.ping()

    def on_game_end(self, data):
        self.logger.info("Race ended")
        self.ping()

    def on_error(self, data):
        self.logger.info("Error: {0}".format(data))
        self.ping()

    # And end here

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn
        }

        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.logger.info("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    logging.config.fileConfig('logging.ini')
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        logger = logging.getLogger()
        host, port, name, key = sys.argv[1:5]
        logger.info("Connecting with parameters:")
        logger.info("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
