import unittest

from tests.fixtures import track_fixture, car_position_fixture

from tracks.track import Track
from car import Car

class CarTestCase(unittest.TestCase):
    def setUp(self):
        track = Track(track_fixture())
        self.car = Car("Bone Wagon", track)

    def test_updating_position(self):
        position = car_position_fixture()
        position["piecePosition"]["pieceIndex"] = 2

        self.car.update(position)
        self.assertEqual(self.car.current_position.piece.index, 2)
        self.assertEqual(self.car.current_position.in_piece_distance, position["piecePosition"]["inPieceDistance"])

    def test_updating_velocity_within_single_piece(self):
        position = car_position_fixture()

        position["piecePosition"]["inPieceDistance"] = 5.0
        self.car.update(position)

        position["piecePosition"]["inPieceDistance"] = 10.0
        self.car.update(position)

        self.assertEqual(self.car.velocity, 5.0)

    def test_updating_velocity_between_two_straights(self):
        position = car_position_fixture()

        position["piecePosition"]["inPieceDistance"] = 50.0
        position["piecePosition"]["pieceIndex"] = 0
        self.car.update(position)

        position["piecePosition"]["inPieceDistance"] = 20.0
        position["piecePosition"]["pieceIndex"] = 1
        self.car.update(position)

        # Straight is 100 long
        self.assertEqual(self.car.velocity, 70.0)

    def test_updating_velocity_between_bend_and_Straight(self):
        position = car_position_fixture()

        position["piecePosition"]["inPieceDistance"] = 50.0
        position["piecePosition"]["pieceIndex"] = 11
        self.car.update(position)

        position["piecePosition"]["inPieceDistance"] = 20.0
        position["piecePosition"]["pieceIndex"] = 12
        self.car.update(position)

        # Shorter lane (-10), so length is 74.6128
        self.assertAlmostEqual(self.car.velocity, 44.6128, 4)


if __name__ == "__main__":
    unittest.main()
