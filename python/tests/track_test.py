import unittest
from tests.fixtures import track_fixture

from tracks.track import Track

class TrackTestCase(unittest.TestCase):
    def test_track_parsing(self):
        track = Track(track_fixture())
        self.assertEqual(len(track.pieces), 40)
        self.assertEqual(len(track.lanes), 2)

    def test_next_piece(self):
        track = Track(track_fixture())
        piece = track.pieces[4]
        self.assertEqual(track.next_piece(piece), track.pieces[5])

        piece = track.pieces[-1]
        self.assertEqual(track.next_piece(piece), track.pieces[0])

if __name__ == "__main__":
    unittest.main()
