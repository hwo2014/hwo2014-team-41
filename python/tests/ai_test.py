import unittest

from tests.fixtures import track_fixture, car_position_fixture
from tracks.track import Track
from car import Car
from ai import AI

class AITestCase(unittest.TestCase):
    def setUp(self):
        track = Track(track_fixture())
        self.car = Car("Bone Wagon", track)
        self.ai = AI(self.car, track)

    def test_update(self):
        self.assertIsNotNone(self.ai.update())

    def test_calculate_desired_speed(self):
        """ Should be less in bend than straight """
        new_position = car_position_fixture()
        new_position["piecePosition"]["pieceIndex"] = 0
        self.car.update(new_position)

        straight_desired_speed = self.ai.calculate_desired_speed()

        new_position["piecePosition"]["pieceIndex"] = 5
        self.car.update(new_position)

        bend_desired_speed = self.ai.calculate_desired_speed()

        self.assertLess(bend_desired_speed, straight_desired_speed)

if __name__ == "__main__":
    unittest.main()
