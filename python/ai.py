import logging
import time

from pid import PID

from tracks.piece import *
from csv_logger import CSVLogger

class AI(object):
    PROP_GAIN = 5.0
    INTEGRAL_GAIN = 1.0
    DERIVATIVE_GAIN = 0.0

    MAX_SPEED = 10 # This is probably something we need to figure out from the system
    MIN_SPEED = 1

    CURVATURE_SCALING_FACTOR = 145

    def __init__(self, car, track):
        self.logger = logging.getLogger()

        self.csv_logger = CSVLogger("ai", ["desired", "current", "cv", "throttle"])

        self.car = car
        self.track = track
        self.desired_speed = AI.MAX_SPEED

        self.pid = PID(AI.PROP_GAIN, AI.INTEGRAL_GAIN, AI.DERIVATIVE_GAIN)
        self.pid.setPoint(self.desired_speed)

    def update(self):
        """
        Returns a tuple (actionType, data) which should be used to send a message
        to the server
        """

        self.update_desired_speed()
        control_variable = self.pid.update(self.car.velocity)

        self.logger.debug("next piece: {}".format(self.track.next_piece(self.car.current_position.piece)))
        self.logger.debug("desired: {}, current: {}, cv: {}".format(
            self.desired_speed, self.car.velocity, control_variable
        ))

        throttle = self.map_control_variable_to_throttle(control_variable)
        self.csv_logger.log(self.desired_speed, self.car.velocity, control_variable, throttle)
        return "throttle", throttle

    def update_desired_speed(self):
        next_desired_speed = self.calculate_desired_speed()
        if next_desired_speed != self.desired_speed:
            self.desired_speed = next_desired_speed
            self.pid.setPoint(self.desired_speed) # This resets pid controller!

    def map_control_variable_to_throttle(self, control_variable):
        if control_variable > 0:
            return min(control_variable / AI.MAX_SPEED, 1.0) # Cap to 1.0
        else:
            return 0

    def calculate_desired_speed(self):
        """
        This is probably where all the magic happens.

        Now just figure out difficulty of the next piece and tune the speed
        accordingly
        """

        next_piece = self.track.next_piece(self.car.current_position.piece)
        scaled_curvature = next_piece.curvature * AI.CURVATURE_SCALING_FACTOR

        if scaled_curvature < 1:
            return AI.MAX_SPEED
        else:
            return max(AI.MAX_SPEED * (1/scaled_curvature), AI.MIN_SPEED)


