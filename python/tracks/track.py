from tracks.piece import Bend, Straight
from tracks.lane import Lane

class Track(object):
    def __init__(self, raw):
        self.pieces = self.parse_track(raw["pieces"])
        self.lanes = self.parse_lanes(raw["lanes"])
        self.total_length = sum([piece.length for piece in self.pieces])

    def next_piece(self, piece):
        return self.pieces[(piece.index + 1) % len(self.pieces)]

    def parse_track(self, pieces):
        parsed_pieces = []
        for i, piece in enumerate(pieces):
            if "length" in piece:
                straight = Straight(i, piece["length"], piece.get("switch", False))
                parsed_pieces.append(straight)
            else:
                bend = Bend(i, piece["radius"], piece["angle"])
                parsed_pieces.append(bend)

        return parsed_pieces

    def parse_lanes(self, lanes):
        parsed_lanes = []
        for i, lane_data in enumerate(lanes):
            lane = Lane(lane_data["distanceFromCenter"], lane_data["index"])
            parsed_lanes.append(lane)

        return parsed_lanes

    def __repr__(self):
        out = "Track. Pieces: [\n"
        for piece in self.pieces:
            out += "\t{0}\n".format(piece)
        out += "]"

        out += "\nLanes:\n"
        for lane in self.lanes:
            out += "\t{0}\n".format(lane)

        return out
