# coding=utf-8
import math

class Piece(object):
    def __init__(self, piece_type, index, length, curvature):
        self.index = index
        self.length = length
        self.piece_type = piece_type
        self.curvature = curvature

    def __repr__(self):
        return "Piece[{0}] length: {1}, type: {2}, curvature: {3}".format(
            self.index, self.length, self.piece_type, self.curvature
        )

class Straight(Piece):
    def __init__(self, index, length, switch):
        super(Straight, self).__init__("Straight", index, length, 0)
        self.switch = switch

    def total_length(self, lane):
        return self.length

    def __repr__(self):
        out = super(Straight, self).__repr__()
        if self.switch:
            out += " with switch"
        else:
            out += " without switch"

        return out

class Bend(Piece):
    def __init__(self, index, radius, angle):
        self.radius = radius
        self.angle = angle
        length = self.calculate_arc(self.angle, self.radius)
        super(Bend, self).__init__("Bend", index, length, self.calculate_curvature(self.radius))

    def total_length(self, lane):
        if self.angle > 0:
            total_radius = self.radius - lane.distance
        else:
            total_radius = self.radius + lane.distance

        return self.calculate_arc(self.angle, total_radius)

    def calculate_arc(self, angle, radius):
        return (math.fabs(angle)/360)* 2 * math.pi * radius # Circle's arc

    def calculate_curvature(self, radius):
        return 1.0 / radius

    def __repr__(self):
        out =  super(Bend, self).__repr__()
        out += ", radius: {0}, angle {1}°".format(self.radius, self.angle)

        return out
