class Lane(object):
    def __init__(self, distance, index):
        self.distance = distance
        self.index = index

    def __repr__(self):
        return "Lane[{0}]: {1} from center".format(self.index, self.distance)
