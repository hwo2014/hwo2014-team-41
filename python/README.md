## Python bot template for HWO-2014

Install (OSX):

    brew install python
    pip install virtualenv

Install (Debian/Ubuntu):

    sudo apt-get install python-pip
    sudo pip install virtualenv

## Tests

Run them from `/python` directory just using python, e.g.

    python tests/car_test.py

## Logging

There is a special logging class `CSVLogger` that can be used to log data used
for later processing.

`CSVLogger` inherits from its parent logger `csv`, and doesn't add a file handler
if the parent is not enabled for DEBUG logging. The default value for `csv`
logger is INFO. These can be configured from `logging.ini` file.

## R graphs

After logging some CSV data using CSVLogger, you can use R-scripts to create
some graphs out of them. There is at least one for AI available:

1. Enable AI-CSV logging by setting csv logger to debug in logging.ini
2. Run a game
3. Run `Rscript r/ai.r <ai.json>`
4. Open ai.pdf
