from tracks.lane import Lane

class Car(object):
    def __init__(self, name, track):
        self.track = track
        self.name = name

        self.current_position = Position(track.pieces[0], 0)
        self.velocity = 0.0

    def update(self, data):
        self.update_velocity(data)
        self.update_position(data)

    def update_velocity(self, data):
        piece = self.track.pieces[data["piecePosition"]["pieceIndex"]]
        piece_distance = data["piecePosition"]["inPieceDistance"]
        lap = data["piecePosition"]["lap"]
        lane = self.track.lanes[data["piecePosition"]["lane"]["startLaneIndex"]]

        end_position = Position(piece, piece_distance, lap)

        self.velocity = self.displacement(self.current_position, end_position, lane)

    def update_position(self, data):
        piece = self.track.pieces[data["piecePosition"]["pieceIndex"]]
        in_piece_distance = data["piecePosition"]["inPieceDistance"]
        lap = data["piecePosition"]["lap"]

        self.current_position = Position(piece, in_piece_distance, lap)

    def displacement(self, start_position, end_position, lane):
        if start_position.piece.index == end_position.piece.index:
            return end_position.in_piece_distance - start_position.in_piece_distance
        else:
            displacement_in_piece = start_position.piece.total_length(lane) - start_position.in_piece_distance
            next_piece = self.track.next_piece(start_position.piece)

            return displacement_in_piece + self.displacement(
                Position(next_piece, 0, start_position.lap), end_position, lane
            )

    def __repr__(self):
        return "Car: {0}".format(self.name)

class Position(object):
    def __init__(self, piece, in_piece_distance, lap=0):
        self.piece = piece
        self.in_piece_distance = in_piece_distance
        self.lap = lap

    def __repr__(self):
        return "Position, piece: {}, distance: {}, lap: {}".format(self.piece, self.in_piece_distance, self.lap)
