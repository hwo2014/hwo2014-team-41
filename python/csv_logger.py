import logging
import time

class CSVLogger(object):
    def __init__(self, name, headers = []):
        self.logger = logging.getLogger("csv.{}".format(name))
        self.logger.setLevel(logging.DEBUG)
        if logging.getLogger("csv").isEnabledFor(logging.DEBUG):
            handler = logging.FileHandler("{}_{}.csv".format(name, int(time.time())))
        else:
            handler = logging.NullHandler()

        self.logger.addHandler(handler)
        self.logger.debug(",".join(headers))

    def log(self, *values):
        message = ",".join(["{}"]*len(values)).format(*values)
        self.logger.debug(message)
